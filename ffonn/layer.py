import json


def as_flat_array(data):
    return data.view(-1)


def as_flat_c_array(data):
    var = as_flat_array(data).detach().numpy().tolist()
    out = json.dumps(var)
    to_ret = "{" + out[1:-1] + "}"
    return to_ret


class Layer:
    def __init__(self):
        self.data = []
        self.src = []
        self.containers = []

    def serialize(self, layer, in1, in2, weights, bias, output):
        raise NotImplementedError("implement this layer type, please!")

    def alloc(self):
        return None


class Conv2d(Layer):
    def __init__(self, kernel, bias, padding, stride, dilation):
        super().__init__()
        self.kernel = kernel
        self.bias = bias
        self.padding = padding
        self.stride = stride
        self.dilation = dilation

    def serialize(self, layer, in1, _in2, weights, bias, output):
        assert (self.dilation == (1, 1))
        out_channels = self.kernel.shape[0]
        kx = self.kernel.shape[2]
        ky = self.kernel.shape[3]

        src_1 = "{} = Layer_make_conv_2d({}, ".format(layer, in1)
        src_2 = "{}, {}, {}, {}, {}, {}, {}, ".format(
            out_channels, kx, ky,
            *self.padding, *self.stride)
        src_3 = "{}, {}, {});".format(weights, bias, output)
        self.src = [src_1 + src_2 + src_3]

        self.data.append("FLOAT {}_container[] = {};".format(weights, as_flat_c_array(self.kernel)))
        self.containers.append(weights)
        if self.bias is not None and max(abs(self.bias)) > 1e-9:
            self.data.append("FLOAT {}_container[] = {};".format(bias, as_flat_c_array(self.bias)))
            self.containers.append(bias)
        else:
            self.data.append("FLOAT* {} = 0;".format(bias))


class Linear(Layer):
    def __init__(self, kernel, bias):
        super().__init__()
        self.kernel = kernel
        self.bias = bias

    def serialize(self, layer, in1, _in2, weights, bias, output):
        src_1 = "{} = Layer_make_dense({}, ".format(layer, in1)
        src_2 = "{}, {}, {}, {});".format(
            self.kernel.shape[0], weights, bias, output)
        self.src = [src_1 + src_2]

        data_1 = "FLOAT {}_container[] = {};".format(weights, as_flat_c_array(self.kernel))
        self.containers.append(weights)

        if self.bias is not None:
            data_2 = "FLOAT {}_container[] = {};".format(bias, as_flat_c_array(self.bias))
            self.containers.append(bias)
        else:
            data_2 = "FLOAT* {} = 0;".format(bias)

        self.data = [data_1, data_2]


class ReLU(Layer):
    # TODO: check inline
    def serialize(self, layer, in1, _in2, weights, bias, output):
        self.src = ["{} = Layer_make_relu({}, {});".format(layer, in1, output)]

    def alloc(self):
        return "inline"


class MaxPool2d(Layer):
    def __init__(self, kernel_size, padding, stride):
        super().__init__()
        self.kernel_size = kernel_size
        self.stride = stride
        # stride = (stride, stride) if type(stride) is int else stride
        assert (padding == 0)

    def serialize(self, layer, in1, _in2, weights, bias, output):
        self.src = ["{} = Layer_make_maxpool({}, {}, {}, {}, {}, {});".format(
            layer, in1, *self.kernel_size, *self.stride, output
        )]


class AvgPool(Layer):
    def __init__(self, kernel_size, padding, stride):
        super().__init__()
        self.kernel_size = kernel_size
        self.stride = stride
        # stride = (stride, stride) if type(stride) is int else stride
        assert (padding == 0)

    def serialize(self, layer, in1, _in2, weights, bias, output):
        self.src = ["{} = Layer_make_avgpool({}, {}, {}, {}, {}, {});".format(
            layer, in1, *self.kernel_size, *self.stride, output
        )]


class Flatten(Layer):
    def serialize(self, layer, in1, _in2, weights, bias, output):
        self.src = ["{} = Layer_make_flatten({}, {});".format(layer, in1, output)]

    def alloc(self):
        return "inline"


class Cat(Layer):
    def serialize(self, layer, in1, in2, weights, bias, output):
        self.src = ["{} = Layer_make_concat({}, {}, {});".format(layer, in1, in2, output)]

    def alloc(self):
        return "cat"


class Skip(Layer):
    def serialize(self, layer, in1, in2, weights, bias, output):
        self.src = ["{} = Layer_make_skip({}, {});".format(layer, in1, output)]

    def alloc(self):
        return "inline"


