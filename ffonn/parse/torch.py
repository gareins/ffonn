from .interface import Network
from ffonn.layer import *

import numpy as np
from functools import reduce
from operator import mul
from itertools import product


import torch
SKIP_MODULES = [torch.nn.Dropout]


class TorchParser(Network):
    def __init__(self, module, input_var, name):
        super().__init__(name)
        self.module = module
        self.input_var = input_var
        self.input_size = input_var.shape[1:]

    @staticmethod
    def as_tuple(parameter):
        return (parameter, parameter) if type(parameter) is int else parameter

    def parse_module(self, module):
        parameters = list(module.parameters()) if type(module) is not str else None

        if type(module) is torch.nn.Conv2d:
            return Conv2d(parameters[0], parameters[1] if len(parameters) > 1 else None,
                          module.padding, module.stride, module.dilation)

        elif type(module) is torch.nn.Linear:
            return Linear(parameters[0], parameters[1] if len(parameters) > 1 else None)

        elif type(module) is torch.nn.ReLU:
            return ReLU()

        elif type(module) is torch.nn.MaxPool2d:
            assert(module.padding == 0)
            return MaxPool2d(self.as_tuple(module.kernel_size), module.padding, self.as_tuple(module.stride))

        elif type(module) is torch.nn.AvgPool2d:
            assert (module.padding == 0)
            return MaxPool2d(self.as_tuple(module.kernel_size), module.padding, self.as_tuple(module.stride))

        elif module == "Flatten":
            return Flatten()

        elif module == "Cat":
            return Cat()

        elif module.__class__ in SKIP_MODULES or module == "Out":
            return Skip()

        else:
            raise NotImplementedError("Layer not implemented: {}".format(module))

    def generate(self):
        forward_list = create_forward_list(self.module, self.input_var)
        outputs, graph = parse_forward_list(forward_list, self.input_var)

        self.outputs = [x.detach().numpy() for x in outputs[:-1]]

        input_size = self.input_var.shape[1:]
        self.allocator.record_new(-1, np.prod(input_size))

        for module, inputs, output, out_size in graph:
            layer = self.parse_module(module)
            self.add_layer(layer, inputs, output, out_size)


def forward_hook_functor():
    """
    Creates function for hook in forward pass through pytorch module
    :return: function to collect data from forward pass
    """
    forward_list = []
    hooked_modules = set()

    # forward hook appends module and its inputs and output
    def forward_hook(mod, _in, _out):
        forward_list.append((mod, _in, _out))

    # function that sets forward hook to a module
    def set_forward_hook(module):
        if id(module) in hooked_modules:
            return

        hooked_modules.add(id(module))
        module.register_forward_hook(forward_hook)
        if hasattr(module, 'inplace'):
            setattr(module, 'inplace', False)

        for m in module.modules():
            set_forward_hook(m)

    return forward_list, set_forward_hook


def create_forward_list(module, var):
    """
    Forwards var through module
    :param module: pytorch module
    :param var: pytorch variable to pass through module
    :return: forward list of variables and operations when pushing var through module
    """
    forward_list, set_forward_hook = forward_hook_functor()
    set_forward_hook(module)

    out_var = module(var)

    forward_list.append(("Out", [out_var], None))
    return forward_list


# sometime our intensor is unknown, so we need to determine, how it was obtained
# we try to find flattening and concating operations
def detect_operation(intensor, outputs):
    # check if flattening
    for o in outputs:
        size_out = reduce(mul, outputs[-1].shape, 1)
        size_in = reduce(mul, intensor.shape, 1)
        if size_in == size_out and intensor.shape == (1, size_in):
            # looks like flattening to me...
            return ['Flatten', [o], intensor]

    # check if concatenation
    for o1, o2 in product(outputs, outputs):
        shapes_match = o1.shape[2] == intensor.shape[2] and o1.shape[3] == intensor.shape[3] and \
            o2.shape[2] == intensor.shape[2] and o2.shape[3] == intensor.shape[3]
        if not shapes_match:
            continue

        if not o1.shape[1] + o2.shape[1] == intensor.shape[1]:
            continue

        probable_input = torch.cat((o1, o2), 1)
        if probable_input.equal(intensor):
            return ["Cat", [o1, o2], intensor]

        probable_input = torch.cat((o2, o1), 1)
        if probable_input.equal(intensor):
            return ["Cat", [o2, o1], intensor]

    return None


def parse_forward_list(forward_list, input_tensor):
    """
    Takes forward list created when passing a variable through
    pytorch module and creates graph of operations

    :param forward_list: list of modules, inputs and outputs
    :param input_tensor: tensor, that was input to module
    :return: computation graph
    """
    outputs = [input_tensor]
    output_graph = []

    integrated_modules = dir(torch.nn)
    integrated_modules.remove('Sequential')
    integrated_modules.remove('ModuleList')

    for mod, intensor, outtensor in forward_list:
        assert(len(intensor) == 1)
        intensor = intensor[0]

        if mod.__class__.__name__ not in integrated_modules and mod is not "Out":
            continue

        input_exists = id(intensor) in [id(out) for out in outputs]
        if not input_exists:
            new_graph_node = detect_operation(intensor, outputs)
            if new_graph_node is None:
                # err :(
                raise ValueError("Did not detect flatten/cat and cannot connect output of one "
                                 "layer with input of another. So exiting...")

            output_graph.append(new_graph_node)
            outputs.append(intensor)

        output_graph.append([mod, [intensor], outtensor])
        outputs.append(outtensor)

    index_tensor = lambda ten: [id(out) for out in outputs].index(id(ten)) - 1
    for idx in range(len(output_graph)):
        output_graph[idx][1] = [index_tensor(inpt) for inpt in output_graph[idx][1]]

        out_var = output_graph[idx][2]
        out_size = np.prod(out_var.shape) if out_var is not None else 0
        output_graph[idx][2] = index_tensor(out_var)
        output_graph[idx].append(out_size)

    # remove "out" from graph
    del output_graph[-1]
    return outputs, output_graph
