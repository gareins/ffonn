import unittest
import test.torch_modules


if __name__ == '__main__':
    test.torch_modules.generate_tests()
    unittest.main(module='test', verbosity=2)
