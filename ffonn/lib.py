def write_file(network, file):
    src = network.get_src()
    data = network.get_data_c()

    if len(src) == 0 or len(data) == 0:
        raise ValueError("src/data do not exist. Did you generate/serialize the network?")

    with open("src/ffonn.h", "r") as fp:
        ffonn_h = fp.read()

    with open("src/ffonn.c", "r") as fp:
        ffonn_c = fp.read()

    ffonn_c = ffonn_c.replace("#include \"ffonn.h\"", "")

    with open(file, "w") as fp:
        print(ffonn_h, file=fp)
        print(ffonn_c, file=fp)
        print(data, file=fp)
        print(src, file=fp)


def write_file_4(network):
    src = network.get_src()
    data_c = network.get_data_c()
    data_h = network.get_data_h()
    if len(src) == 0 or len(data_h) == 0:
        raise ValueError("src/data do not exist. Did you generate/serialize the network?")

    file_data_h = network.name + '_data.h'
    file_src_h = network.name + '.h'

    with open(file_data_h, "w") as fp:
        print("#ifndef {}".format(file_data_h.replace(".", "_")), file=fp)
        print("#define {}".format(file_data_h.replace(".", "_")), file=fp)

        print("#include \"ffonn.h\"", file=fp)
        print("#include \"{}\"".format(file_data_h), file=fp)
        print("", file=fp)
        print(data_h, file=fp)

        print("#endif", file=fp)

    with open(network.name + '_data.c', "w") as fp:
        print("#include \"ffonn.h\"", file=fp)
        print("", file=fp)
        print(data_c, file=fp)

    with open(network.name + ".c", "w") as fp:
        print("#include \"ffonn.h\"", file=fp)
        print("#include \"{}\"".format(file_data_h), file=fp)
        print("#include \"{}\"".format(file_src_h), file=fp)
        print("", file=fp)
        print(src, file=fp)

    with open(file_src_h, "w") as fp:
        print("#ifndef {}".format(file_src_h.replace(".", "_")), file=fp)
        print("#define {}".format(file_src_h.replace(".", "_")), file=fp)

        print("#include \"ffonn.h\"", file=fp)
        print("", file=fp)
        print("{};\n".format(network.network_make_prototype()), file=fp)

        print("#endif", file=fp)


if __name__ == '__main__':
    from parse import TorchParser
    from torchvision import models
    import torch

    squeezenet = models.squeezenet1_1(pretrained=True)
    var = torch.autograd.Variable(torch.randn(1, 3, 224, 224))

    network = TorchParser(squeezenet, var, "sq11")
    network.generate()
    network.serialize()
    write_file_4(network)



