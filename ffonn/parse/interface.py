from ffonn.allocator import FirstFit


class Network:
    def __init__(self, name, allocator=None):
        self.name = name
        self.layers = []
        self.allocator = FirstFit() if allocator is None else allocator

        self.input_size = None
        self.src, self.data_c, self.data_h = [], [], []
        self.outputs = []

    def add_layer(self, layer, inputs, output, out_size):
        self.layers.append((layer, inputs, output))

        alloc = layer.alloc()
        # recording allocations
        if alloc is None:
            self.allocator.record_new(output, out_size)
            for inpt in inputs:
                self.allocator.record_use(inpt)
        elif alloc == "cat":
            self.allocator.record_cat(output, inputs[0], inputs[1])
        elif alloc == "inline":
            self.allocator.recode_inline(output, inputs[0])
        else:
            raise RuntimeError("Non familiar layer alloc")

    @staticmethod
    def network_input():
        return "network_input"

    def get_input(self, idx):
        if idx == -1:
            return self.network_input()
        elif idx >= 0:
            return "Layer_output({}_layers[{}])".format(self.name, idx)
        else:
            raise ValueError("Not allowed: ", idx)

    def serialize(self):
        assert(self.input_size is not None and len(self.input_size) == 3)

        self.src.append("*ok = 0;")
        self.src.append("if(!EXECUTION_OK) return Network_make(Mat_make_empty(), 0, NULL);")
        self.src.append("Mat_t {} = Mat_make_3d({}, {}, {}, {}_input);".format(
            self.network_input(), *self.input_size, self.name))

        containers = []

        for idx, (layer, inputs, output) in enumerate(self.layers):
            weights_c = "{}_weight_{:02}".format(self.name, idx)
            bias_c = "{}_bias_{:02}".format(self.name, idx)
            layer_c = "{}_layers[{}]".format(self.name, idx)
            output_c = "{}_alloc_{:02d}".format(self.name, idx)

            li = self.get_input(inputs[0])
            li2 = self.get_input(inputs[1]) if len(inputs) > 1 else None

            layer.serialize(layer_c, li, li2, weights_c, bias_c, output_c)

            self.src.extend(layer.src)
            self.data_c.extend(layer.data)
            containers.extend(layer.containers)

        self.data_c.append('')
        self.data_h.append('')
        self.data_h.append('// Layers container')

        layer_container = "{}_layers".format(self.name)
        self.data_c.append("Layer_t {}_container[{}] = {{0}};".format(layer_container, len(self.layers)))
        self.data_c.append("Layer_t* {} = {}_container;".format(layer_container, layer_container))
        self.data_h.append("Layer_t* {};".format(layer_container))

        self.data_c.append('')
        self.data_h.append('')
        self.data_h.append('// Weight containers')
        for container in containers:
            self.data_h.append('FLOAT* {};'.format(container))
            self.data_c.append('FLOAT* {} = {}_container;'.format(container, container))

        self.data_c.append('')
        self.data_h.append('')
        self.data_h.append('// Network containers')

        allocs_c, allocs_h = self.allocator.allocate(self.name)
        self.data_h.extend(allocs_h)
        self.data_c.extend(allocs_c)

        self.src.append("Network_t network = Network_make({}, {}, {}_layers);".format(
            self.network_input(), len(self.layers), self.name
        ))
        self.src.append("*ok = 1;")
        self.src.append("return network;")

    def network_make_prototype(self):
        return "Network_t {}_make(int* ok)".format(self.name)

    def get_src(self):
        if len(self.src) == 0:
            return ""

        src_1 = "{} {{  \n  ".format(self.network_make_prototype())
        src_2 = "\n  ".join(self.src)
        src_3 = "\n}"

        return src_1 + src_2 + src_3

    def get_data_c(self):
        return "\n".join(self.data_c)

    def get_data_h(self):
        return "\n".join(self.data_h)

    def generate(self):
        raise NotImplementedError("Not implemented in base class")
