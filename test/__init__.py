import unittest
import tempfile


class TestGenerator(unittest.TestCase):
    def setUp(self):
        self.folder = tempfile.mkdtemp()

    def tearDown(self):
        if hasattr(self, 'folder'):
            print("Error code in folder: {}".format(self.folder))


def add_test(name, func):
    assert(not hasattr(TestGenerator, name))
    setattr(TestGenerator, "test_" + name, func)


