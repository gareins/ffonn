import subprocess

import numpy as np
import torch
from . import add_test

from ffonn.lib import write_file
from ffonn.parse import TorchParser
from ffonn import as_flat_c_array


def generate_test(name, module, input_shape, raise_exec=None):
    def test(self):
        var = torch.autograd.Variable(torch.randn(1, *input_shape))

        # 1) generate C code
        network = TorchParser(module, var, name)
        network.generate()
        network.serialize()

        file_net = self.folder + "/" + name + ".h"
        write_file(network, file_net)

        file_test = self.folder + "/test.c"
        with open("src/test.c", "r") as fp:
            test_c = fp.read()

        test_c = test_c.replace("__NETWORK_NAME__", name)
        test_c = test_c.replace("__NETWORK_INPUT__", as_flat_c_array(var))
        test_c = "#define FLOAT_FORMAT \"%.7f\"\n#include \"{}\"\n{}".format(file_net, test_c)

        with open(file_test, "w") as fp:
            print(test_c, file=fp)

        # 2) build C code
        file_exec = self.folder + "/a.out"
        proc = subprocess.run(["gcc", "-std=c99", file_test, "-o", file_exec, "-lm"])
        self.assertEqual(proc.returncode, 0)

        # 3) run C code
        proc = subprocess.run([file_exec], stdout=subprocess.PIPE)

        # 4) check C output
        vectors_c = []
        for line in proc.stdout.decode('ascii').splitlines():
            self.assertEqual(line[0], '[')
            self.assertEqual(line[-1], ']')

            new_vec = [float(x) for x in line[1:-1].split()]
            vectors_c.append(new_vec)

        # 5) compare with pytorch output
        self.assertEqual(len(network.outputs), len(vectors_c))
        # err = False
        for idx, (tv, cv) in enumerate(zip(network.outputs, vectors_c)):
            # if idx != 0:
            #     print(tv.flatten())
            #     print(cv)
            #
            err = tv.flatten() - np.array(cv)
            # print(err)
            # if np.linalg.norm(err) > 1e-3:
            #     err = True
            self.assertLess(np.linalg.norm(err), 1e-3, msg="At layer: {}".format(idx - 1))

        # if err:
        #     self.assertEqual(1, 0)
        delattr(self, 'folder')

    if raise_exec is not None:
        test_func = test

        def test(self):
            self.assertRaises(raise_exec, lambda: test_func(self))
            delattr(self, 'folder')  # raised, so need to del self.folder for tearDown...

    add_test(name, test)


class ExampleConcat(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.conv = torch.nn.Conv2d(1, 1, 1)
        self.acti = torch.nn.ReLU()

    def forward(self, inpt):
        in1 = self.conv(inpt)
        in2 = self.acti(inpt)
        return torch.cat((in1, in2), 1)



def generate_tests():
    generate_test("conv2d", torch.nn.Conv2d(2, 3, 3), (2, 10, 10))
    generate_test("conv2d_kernel", torch.nn.Conv2d(3, 2, (2, 3)), (3, 5, 5))
    generate_test("conv2d_stride", torch.nn.Conv2d(3, 2, 3, stride=(1, 2)), (3, 5, 5))
    generate_test("conv2d_padding", torch.nn.Conv2d(3, 2, 3, padding=(2, 1)), (3, 5, 5))
    generate_test("conv2d_bias", torch.nn.Conv2d(3, 2, 1, bias=False), (3, 8, 9))
    generate_test("conv2d_dilation", torch.nn.Conv2d(3, 2, 2, dilation=2), (3, 5, 5), raise_exec=AssertionError)

    generate_test("linear", torch.nn.Linear(10, 3), (1, 1, 10))
    generate_test("linear_bias", torch.nn.Linear(3, 10, bias=False), (1, 1, 3))

    generate_test("maxpool", torch.nn.MaxPool2d(2), (1, 5, 5))
    generate_test("maxpool_padding", torch.nn.MaxPool2d(2, padding=(1, 0)), (1, 5, 5), raise_exec=AssertionError)
    generate_test("maxpool_stride", torch.nn.MaxPool2d(2, stride=(2, 1)), (1, 5, 6))

    # generate_test("concat", ExampleConcat(), (1, 3, 3))

