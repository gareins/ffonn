class LifeTime:
    def __init__(self, size, start, end=-1, aliases=None):
        self.size = size
        self.start = start
        self.end = end
        self.aliases = [] if aliases is None else aliases[:]
        self.offset = None

    def add_alias(self, alias, offset=0):
        self.aliases.append((alias, offset))

    def __str__(self):
        return str({"SIZE": self.size,
                    "START": self.start,
                    "END:": self.end,
                    "OFFSET": self.offset,
                    "ALIASES": self.aliases})

    def __repr__(self):
        return str(self)


class Allocator:
    def __init__(self):
        self.recording = []

        self.lifetimes = None
        self.max_size = None

    def record_new(self, idx, size):
        self.recording.append(("new", (idx, size)))

    def record_use(self, idx):
        self.recording.append(("use", idx))

    def recode_inline(self, in_idx, out_idx):
        self.recording.append(("inl", (in_idx, out_idx)))

    def record_cat(self, out_idx, in_idx1, in_idx2):
        self.recording.append(("cat", (out_idx, in_idx1, in_idx2)))

    def _base_lifetimes(self):
        lifetimes = {}
        alias = {}
        cats = {}

        def use(p):
            lifetimes[alias[p]].end = time
            if p in cats:
                use(cats[p][0])
                use(cats[p][1])

        # print(self.recording)

        for time, (typ, data) in enumerate(self.recording):

            if typ == "new":
                name, size = data
                lifetimes[name] = LifeTime(size, time)
                alias[name] = name

            elif typ == "use":
                use(data)

            elif typ == "inl":
                p1, p2 = data
                alias[p1] = alias[p2]
                use(p1)

            elif typ == "cat":
                n, p1, p2 = data
                alias[n] = alias[p1]
                cats[n] = (alias[p1], alias[p2])
                use(n)

        import time
        time.sleep(0.1)

        # pack aliases in lifetimes
        for src, dest in alias.items():
            lifetimes[dest].add_alias(src)

        # get rid of concatenations
        for p1, p2 in cats.values():
            lt1 = lifetimes[p1]
            lt2 = lifetimes[p2]

            if lt1.start > lt2.start:
                lt1, lt2 = lt2, lt1

            new_aliases = lt1.aliases + [(a, ps + lt1.size) for a, ps in lt2.aliases]
            del lifetimes[p1]
            del lifetimes[p2]
            lifetimes[p1] = LifeTime(lt1.size + lt2.size, lt1.start, max(lt1.end, lt2.end), new_aliases)

        return list(sorted(lifetimes.values(), key=lambda x: x.start))

    def fit(self):
        raise NotImplementedError("Base class does not implement this...")

    def allocate(self, network_name):
        self.lifetimes = self._base_lifetimes()
        self.max_size = self.fit()
        # self.visualize()
        return self.encode_allocation(network_name)

    def visualize(self):
        fills = "+*xo@#%"
        width, height = 30, 20
        matrix = [[0 for _x in range(width)] for _y in range(height)]
        # print(self.lifetimes)

        dw = width / max(lt.end for lt in self.lifetimes)
        dh = height / self.max_size

        for idx, lt in enumerate(self.lifetimes):
            x = int(round(lt.start * dw))
            w = int(round((lt.end - lt.start) * dw))
            y = int(round(lt.offset * dh))
            h = int(round(lt.size * dh))

            # print(x, y, w, h, dw, dh)
            fill = idx % len(fills)

            for mx in range(x, x + w):
                for my in range(y, y + h):
                    # assert(matrix[mx][my] == 0)
                    matrix[my][mx] = fills[fill]

        for row in matrix:
            for el in row:
                print(' ' if el == 0 else el, end='')
            print('')

    def encode_allocation(self, network_name):
        src = ["FLOAT {}_float_store[{}] = {{0}};".format(network_name, self.max_size)]
        header = []

        for lt in self.lifetimes:
            for alias, offset in lt.aliases:
                if alias < 0:
                    alias = "input"
                else:
                    alias = "alloc_{:02d}".format(alias)

                header_line = "FLOAT* {}_{}".format(network_name, alias)
                header.append(header_line + ';')
                src.append("{} = &{}_float_store[{}];".format(
                    header_line, network_name, lt.offset + offset))

        return src, header
