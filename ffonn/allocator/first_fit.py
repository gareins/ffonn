from .base import Allocator


class FirstFit(Allocator):
    def fit(self):
        current_chunks = []
        max_size = 0

        for idx, lt in enumerate(self.lifetimes):
            current_chunks = [cc for cc in current_chunks if cc[2] > lt.start]
            current_chunks = list(sorted(current_chunks, key=lambda x:x[1]))

            prev_end = 0
            for cidx, csize, _ in current_chunks:
                if cidx - prev_end >= lt.size:
                    break
                prev_end = cidx + csize

            lt.offset = prev_end
            current_chunks.append((prev_end, lt.size, lt.end))
            max_size = max(max_size, prev_end + lt.size)

        return max_size
