#include <stdlib.h>

FLOAT netin[] = __NETWORK_INPUT__;

int main()
{
    int ok;
    Network_t network = __NETWORK_NAME___make(&ok);
    network.verbose = 1;
    Mat_copy_data(Network_input(network), netin);

    Network_apply(network);
    return 0;
}
