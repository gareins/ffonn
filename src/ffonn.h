#ifndef FFONN_H
#define FFONN_H

#include <stddef.h>
#include <stdint.h>

// SETTINGS ----------------------------
//
// to enable printing (also import stdio)
#define ENABLE_PRINT
//
// by default we use longjmp for error handling, however one can just
// use abort() with this. Beware when using longjmp, can cause undefined
// behaviour very quickly. If generated code creates such a bug, please
// report to developers. Does nothing without assertions enabled
#define DISABLE_LONGJMP
//
// to enable all check around the code
#define ENABLE_ASSERTIONS
//
// by default we use static allocation, however dynamic is available via
//#define ENABLE_DYNAMIC_ALLOCATION
//
// -------------------------------------

#ifdef USE_DOUBLE
#define FLOAT double
#define POW pow
#define EXP exp
#define LOG log
#define FABS fabs
#else
#define FLOAT float
#define POW powf
#define EXP expf
#define LOG logf
#define FABS fabsf
#endif

#ifndef INT
#define INT int32_t
#endif

// static assert
#define STATIC_ASSERT(COND,MSG) typedef char static_assertion_##MSG[(COND)?1:-1]
STATIC_ASSERT(sizeof(INT) <= sizeof(FLOAT), Size_of_INT_should_be_less_or_equal_to_size_of_FLOAT);

/* Error handling with longjmp stuff */
#ifndef DISABLE_LONGJMP
#include <setjmp.h>
extern jmp_buf FFONN_JUMP;
#define ABORT() { longjmp(FFONN_JUMP, 1); }
#define EXECUTION_OK !setjmp(FFONN_JUMP)
#else
#define ABORT() {  abort(); }
#define EXECUTION_OK 1
#endif // DISABLE_LONGJMP

typedef struct Mat_t {
    FLOAT* _data;
    INT _dim[4];
} Mat_t;

/* Mat data methods */

Mat_t Mat_flatten(Mat_t mat);
INT Mat_size(Mat_t mat);
FLOAT* Mat_data(Mat_t mat);
void Mat_copy_data(Mat_t mat, FLOAT* raw_data);
INT Mat_is_allocated(Mat_t mat);

Mat_t Mat_make_empty();
Mat_t Mat_make_4d(INT in, INT out, INT height, INT width, FLOAT* data);
Mat_t Mat_make_3d(INT channels, INT height, INT width, FLOAT* data);
Mat_t Mat_make_2d(INT channels, INT length, FLOAT* data);
Mat_t Mat_make_1d(INT length, FLOAT* data);

void Mat_print_4d(Mat_t mat);
void Mat_print_3d(Mat_t mat);
void Mat_print_2d(Mat_t mat);
void Mat_print_1d(Mat_t mat);

INT Mat_dim4d_in(Mat_t mat);
INT Mat_dim4d_out(Mat_t mat);
INT Mat_dim4d_height(Mat_t mat);
INT Mat_dim4d_width(Mat_t mat);
INT Mat_dim3d_channel(Mat_t mat);
INT Mat_dim3d_height(Mat_t mat);
INT Mat_dim3d_width(Mat_t mat);
INT Mat_dim2d_channel(Mat_t mat);
INT Mat_dim2d_length(Mat_t mat);
INT Mat_dim1d(Mat_t mat);

void Mat_set_value_4d(Mat_t mat, INT in, INT out, INT height, INT width, FLOAT val);
void Mat_set_value_3d(Mat_t mat, INT channel, INT height, INT width, FLOAT val);
void Mat_set_value_2d(Mat_t mat, INT channel, INT length, FLOAT val);
void Mat_set_value_1d(Mat_t mat, INT length, FLOAT val);

FLOAT* Mat_get_ptr_4d(Mat_t mat, INT in, INT out, INT height, INT width);
FLOAT* Mat_get_ptr_3d(Mat_t mat, INT channel, INT height, INT width);
FLOAT* Mat_get_ptr_2d(Mat_t mat, INT channel, INT length);
FLOAT* Mat_get_ptr_1d(Mat_t mat, INT length);

/* Mathematical operations on Mats */

void Op_multiply_mat_vec(Mat_t mat, Mat_t vec, Mat_t out);
void Op_sum_matrix_matrix_inplace(Mat_t mat_in, Mat_t mat_in_out);
void Op_sum_matrix_matrix(Mat_t mat1, Mat_t mat2, Mat_t out);

void Op_sum_matrix_vector_dim(Mat_t mat, Mat_t vec, Mat_t out, INT dim);
void Op_sum_matrix_vector_dim0(Mat_t mat, Mat_t vec, Mat_t out);
void Op_sum_matrix_vector_dim1(Mat_t mat, Mat_t vec, Mat_t out);
void Op_sum_matrix_vector_dim2(Mat_t mat, Mat_t vec, Mat_t out);
void Op_sum_matrix_vector_dim3(Mat_t mat, Mat_t vec, Mat_t out);

void Op_sum_matrix_vector_dim_inplace(Mat_t mat, Mat_t vec, INT dim);
void Op_sum_matrix_vector_dim0_inplace(Mat_t mat, Mat_t vec);
void Op_sum_matrix_vector_dim1_inplace(Mat_t mat, Mat_t vec);
void Op_sum_matrix_vector_dim2_inplace(Mat_t mat, Mat_t vec);
void Op_sum_matrix_vector_dim3_inplace(Mat_t mat, Mat_t vec);

void Op_apply_bias(Mat_t mat, Mat_t vec, Mat_t out, INT mat_size_over_vec);
void Op_apply_bias_inplace(Mat_t mat, Mat_t vec, INT mat_size_over_vec);

void Op_maxpool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex);
void Op_avgpool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex);
void Op_lppool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT power);

void Op_constant_pad(Mat_t mat, Mat_t out, FLOAT constant, INT left, INT right, INT up, INT down);
void Op_zero_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down);
void Op_replication_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down);
void Op_reflection_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down);

void Op_convolution_2d(Mat_t input, Mat_t kernel, Mat_t out, INT pad_y, INT pad_x, INT stridey, INT stridex);

FLOAT Op_activate_ELU(FLOAT val, FLOAT* params);
FLOAT Op_activate_HardShrink(FLOAT val, FLOAT* params);
FLOAT Op_activate_HardTanh(FLOAT val, FLOAT* params);
FLOAT Op_activate_LeakyReLU(FLOAT val, FLOAT* params);
FLOAT Op_activate_LogSigmoid(FLOAT val, FLOAT* _);
FLOAT Op_activate_PReLU(FLOAT val, FLOAT* params);
FLOAT Op_activate_ReLU(FLOAT val, FLOAT* _);
FLOAT Op_activate_ReLU6(FLOAT val, FLOAT* params);
FLOAT Op_activate_RReLU(FLOAT val, FLOAT* params);
FLOAT Op_activate_SeLU(FLOAT val, FLOAT* _);
FLOAT Op_activate_Sigmoid(FLOAT val, FLOAT* _);
FLOAT Op_activate_SoftPlus(FLOAT val, FLOAT* params);
FLOAT Op_activate_SoftShrink(FLOAT val, FLOAT* params);
FLOAT Op_activate_SoftSign(FLOAT val, FLOAT* _);
FLOAT Op_activate_Tanh(FLOAT val, FLOAT* _);
FLOAT Op_activate_TanhShrink(FLOAT val, FLOAT* _);
FLOAT Op_activate_Threshold(FLOAT val, FLOAT* params);

/* Layers */

typedef enum {
    LAYER_NONE=0,
    LAYER_Skip,
    LAYER_Conv_2D,
    LAYER_Dense,
    LAYER_ELU,
    LAYER_HardShrink,
    LAYER_HardTanh,
    LAYER_LeakyReLU,
    LAYER_LogSigmoid,
    LAYER_PReLU,
    LAYER_ReLU,
    LAYER_ReLU6,
    LAYER_RReLU,
    LAYER_SeLU,
    LAYER_Sigmoid,
    LAYER_SoftPlus,
    LAYER_SoftShrink,
    LAYER_SoftSign,
    LAYER_Tanh,
    LAYER_TanhShrink,
    LAYER_Threshold,
    LAYER_MaxPool,
    LAYER_AvgPool,
    LAYER_LpPool,
    LAYER_ReplicationPad,
    LAYER_ReflectionPad,
    LAYER_ConstantPad,
    LAYER_ZeroPad,
    LAYER_Flatten,
    LAYER_Concat,
    LAYER_ALL_TYPES,
} layer_type;

#define NUM_LAYER_PARAMS 8

typedef struct Layer_t {
    layer_type _type;

    Mat_t _input;
    Mat_t _output;

    // weights for convolution / dense
    Mat_t _weights;
    // bias / additional input / something else?
    Mat_t _other_mat;
    // padding, stride, other parameters
    FLOAT _parameters[NUM_LAYER_PARAMS];
} Layer_t;

void Layer_print(Layer_t layer);
INT Layer_has_bias(Layer_t layer);
Mat_t Layer_output(Layer_t layer);
Mat_t Layer_input(Layer_t layer);
INT Layer_int_param(Layer_t layer, INT idx);

void Layer_apply(Layer_t layer);

void Layer_set_weights(Layer_t layer, FLOAT* weights);
void Layer_set_bias(Layer_t layer, FLOAT* bias);

Layer_t Layer_make_skip(Mat_t input_container, FLOAT* output);

Layer_t Layer_make_conv_2d(Mat_t input_container, INT out_channels, INT kernely, INT kernelx,
                           INT paddingy, INT paddingx, INT stridey, INT stridex,
                           FLOAT* weights, FLOAT* bias, FLOAT* output);
Layer_t Layer_make_dense(Mat_t input_container, INT out_size, FLOAT* weights, FLOAT* bias, FLOAT* output);

Layer_t Layer_make_elu(Mat_t input_container, FLOAT alpha, FLOAT* output);
Layer_t Layer_make_hardshrink(Mat_t input_container, FLOAT lambda, FLOAT* output);
Layer_t Layer_make_hardtanh(Mat_t input_container, FLOAT min_val, FLOAT max_val, FLOAT* output);
Layer_t Layer_make_leakyrelu(Mat_t input_container, FLOAT negative_slope, FLOAT* output);
Layer_t Layer_make_logsigmoid(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_prelu(Mat_t input_container, FLOAT a, FLOAT* output);
Layer_t Layer_make_relu(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_relu6(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_rrelu(Mat_t input_container, FLOAT lower, FLOAT upper, FLOAT* output);
Layer_t Layer_make_selu(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_sigmoid(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_softplus(Mat_t input_container, FLOAT beta, FLOAT threshold, FLOAT* output);
Layer_t Layer_make_softshrink(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_softsign(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_tanh(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_tanhshrink(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_threshold(Mat_t input_container, FLOAT threshold, FLOAT value, FLOAT* output);

Layer_t Layer_make_maxpool(Mat_t input_container, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output);
Layer_t Layer_make_avgpool(Mat_t input_container, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output);
Layer_t Layer_make_lppool(Mat_t input_container, FLOAT power, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output);

Layer_t Layer_make_replication_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output);
Layer_t Layer_make_reflection_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output);
Layer_t Layer_make_constant_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT constant, FLOAT* output);
Layer_t Layer_make_zero_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output);

Layer_t Layer_make_flatten(Mat_t input_container, FLOAT* output);
Layer_t Layer_make_concat(Mat_t m1, Mat_t m2, FLOAT* output);

/* Neural network data structure and functions */

typedef struct Network_t {
    Mat_t input;
    Layer_t* layers;
    INT size;
    INT verbose;
} Network_t;

Network_t Network_make(Mat_t input, INT size, Layer_t* layer_store);
INT Network_apply(Network_t network);
Mat_t Network_input(Network_t network);
Mat_t Network_output(Network_t network);
void Network_print(Network_t network);

#endif
