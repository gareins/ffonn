#include "ffonn.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

#define UNUSED(x) (void)(x)
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

const char* LAYER_STRING_FORMAT = "%10s -> %12s, [%s]\n";

#ifndef FLOAT_FORMAT
#define FLOAT_FORMAT "%.3f"
#endif

/* Longjmp stuff */
#ifndef DISABLE_LONGJMP
jmp_buf FFONN_JUMP;
#endif

/* Printing stuff */
void print_local(const char* format, ...);
void sprintf_local(char* file, const char* format, ...);

#ifndef ENABLE_PRINT
void print_local(const char* format, ...) {UNUSED(format);}
void sprintf_local(char* file, const char* format, ...) {UNUSED(format); UNUSED(file);}
const char* layer_names[LAYER_ALL_TYPES] = {0};
#else
#include <stdio.h>
#include <stdarg.h>
void print_local(const char* format, ...) {
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
  fflush(stdout);
}
void sprintf_local(char* file, const char* format, ...) {
  va_list args;
  va_start(args, format);
  vsprintf(file, format, args);
  va_end(args);
}
const char* layer_names[LAYER_ALL_TYPES] = {
    "NONE", "Skip", "Conv_2D", "Dense",
    "ELU", "HardShrink", "HardTanh", "LeakyReLU",
    "LogSigmoid", "PReLU", "ReLU", "ReLU6", "RReLU",
    "SeLU", "Sigmoid", "SoftPlus", "SoftShrink",
    "SoftSign", "Tanh", "TanhShrink", "Threshold",
    "MaxPool", "AvgPool", "LpPool", "ReplicationPad",
    "ReflectionPad", "ConstantPad", "ZeroPad",
    "Flatten", "Concat"
};

#endif // ENABLE_PRINT
#define PRINTF print_local
#define SPRINTF sprintf_local

/* OpenMP stuff */
#ifdef _OPENMP
   #include <omp.h>
#else
   #define omp_get_thread_num() 0
#endif

/* Allocation stuff */
#ifdef ENABLE_DYNAMIC_ALLOCATION
FLOAT* ALLOC_FLOAT(INT n) (FLOAT*)malloc((size_t)n * sizeof(FLOAT));ALLOC_FLOAT
#else
FLOAT* ALLOC_FLOAT(INT _) { UNUSED(_); PRINTF("DISABLED DYNAMIC ALLOCATION!"); ABORT(); return (FLOAT*)NULL; }
#endif // ENABLE_DYNAMIC_ALLOCATION

#ifndef ENABLE_ASSERTIONS
#define ASSERT(b) UNUSED(b)
#define ASSERT_EQ(v1, v2) { UNUSED(v1); UNUSED(v2); }
#else
#define ASSERT(b) if(!(b)) { PRINTF("ERR: %d @ %s:%d\n", b, __FILE__, __LINE__); ABORT(); }
#define ASSERT_EQ(v1, v2) if(v1 != v2) { PRINTF("ERR: %d != %d @ %s:%d\n", v1, v2, __FILE__, __LINE__); ABORT(); }
#endif // ENABLE_ASSERTIONS

#define MATCHK(mat) { ASSERT((mat)._data != NULL); }
#define MATCHK_4D(mat) { MATCHK(mat); }
#define MATCHK_3D(mat) { MATCHK(mat); ASSERT_EQ((mat)._dim[0], 1); }
#define MATCHK_2D(mat) { MATCHK(mat); ASSERT_EQ((mat)._dim[0], 1); ASSERT_EQ((mat)._dim[1], 1); }
#define MATCHK_1D(mat) { MATCHK(mat); ASSERT_EQ((mat)._dim[0], 1); ASSERT_EQ((mat)._dim[1], 1); ASSERT_EQ((mat)._dim[2], 1); }
#define MAT_SAME_SIZE(mat1, mat2) { ASSERT_EQ((mat1)._dim[0], (mat2)._dim[0]); \
                                    ASSERT_EQ((mat1)._dim[1], (mat2)._dim[1]); \
                                    ASSERT_EQ((mat1)._dim[2], (mat2)._dim[2]); \
                                    ASSERT_EQ((mat1)._dim[3], (mat2)._dim[3]); }

#define UNREACHABLE() { PRINTF("UNREACHABLE: @ %s:%d\n", __FILE__, __LINE__); ABORT(); }

/***********************************************************************************/
/************************************* MAT *****************************************/
/***********************************************************************************/

Mat_t Mat_flatten(Mat_t mat) {
    MATCHK(mat);
    INT size = Mat_size(mat);

    Mat_t to_ret = mat;
    to_ret._dim[0] = 1;
    to_ret._dim[1] = 1;
    to_ret._dim[2] = 1;
    to_ret._dim[3] = size;
    return to_ret;
}

INT Mat_size(Mat_t mat) {
    MATCHK(mat);
    return mat._dim[0] * mat._dim[1] * mat._dim[2] * mat._dim[3];
}

FLOAT* Mat_data(Mat_t mat) {
    return mat._data;
}

void Mat_copy_data(Mat_t mat, FLOAT* raw_data) {
    MATCHK(mat);
    memcpy(mat._data,
            raw_data,
            (size_t)Mat_size(mat) * sizeof(FLOAT));
}

INT Mat_is_allocated(Mat_t mat) {
    return mat._data != NULL;
}

Mat_t Mat_make_empty() {
    Mat_t to_ret = {NULL, {0, 0, 0, 0}};
    return to_ret;
}

Mat_t Mat_make_4d(INT in, INT out, INT height, INT width, FLOAT* data) {
    ASSERT(in > 0);
    ASSERT(out > 0);
    ASSERT(height > 0);
    ASSERT(width > 0);

    if(data == NULL) {
        data = ALLOC_FLOAT(in * out * width * height);
    }
    Mat_t to_ret = {data, {in, out, height, width}};
    return to_ret;
}

Mat_t Mat_make_3d(INT channels, INT height, INT width, FLOAT* data) {
    return Mat_make_4d(1, channels, height, width, data);
}

Mat_t Mat_make_2d(INT channels, INT length, FLOAT* data) {
    return Mat_make_4d(1, 1, channels, length, data);
}

Mat_t Mat_make_1d(INT length, FLOAT* data) {
    return Mat_make_4d(1, 1, 1, length, data);
}

void _Mat_print_1d(FLOAT* mat, INT num, const char* prefix) {
    PRINTF("%s[", prefix);
    for(INT i = 0; i < num; i++) {
        PRINTF(FLOAT_FORMAT " ", mat[i]);
    }
    PRINTF("]\n");
}

void Mat_print_4d(Mat_t mat) {
    MATCHK_4D(mat);

    INT idx = 0;
    INT width = Mat_dim4d_width(mat);

    PRINTF("[");
    for(INT i = 0; i < Mat_dim4d_out(mat); i++) {
        PRINTF(i == 0 ? "[" : " [");
        for(INT c = 0; c < Mat_dim4d_in(mat); c++) {
            PRINTF(c == 0 ? "[" : "  [");
            for(INT y = 0; y < Mat_dim4d_height(mat); y++) {
                _Mat_print_1d(&mat._data[idx], width, y == 0 ? "" : "   ");
                idx += width;
            }
            PRINTF("]\n");
        }
        PRINTF("]\n");
    }
    PRINTF("]\n");
}

void Mat_print_3d(Mat_t mat) {
    MATCHK_3D(mat);

    INT idx = 0;
    INT width = Mat_dim3d_width(mat);

    PRINTF("[");
    for(INT c = 0; c < Mat_dim3d_channel(mat); c++) {
        PRINTF(c == 0 ? "[" : " [");
        for(INT y = 0; y < Mat_dim3d_height(mat); y++) {
            _Mat_print_1d(&mat._data[idx], width, y == 0 ? "" : "  ");
            idx += width;
        }
        PRINTF("]\n");
    }
    PRINTF("]\n");
}

void Mat_print_2d(Mat_t mat) {
    MATCHK_2D(mat);

    INT idx = 0;
    INT width = Mat_dim2d_length(mat);

    PRINTF("[");
    for(INT y = 0; y < mat._dim[2]; y++) {
        _Mat_print_1d(&mat._data[idx], width, y == 0 ? "" : " ");
        idx += width;
    }
    PRINTF("]\n");
}

void Mat_print_1d(Mat_t mat) {
    MATCHK_1D(mat);

    _Mat_print_1d(mat._data, Mat_dim1d(mat), "");
}

INT Mat_dim4d_out(Mat_t mat) {
    return mat._dim[0];
}

INT Mat_dim4d_in(Mat_t mat) {
    return mat._dim[1];
}

INT Mat_dim4d_height(Mat_t mat) {
    return mat._dim[2];
}

INT Mat_dim4d_width(Mat_t mat) {
    return mat._dim[3];
}

INT Mat_dim3d_channel(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    return mat._dim[1];
}

INT Mat_dim3d_height(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    return mat._dim[2];
}

INT Mat_dim3d_width(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    return mat._dim[3];
}

INT Mat_dim2d_channel(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    ASSERT_EQ(mat._dim[1], 1);
    return mat._dim[2];
}

INT Mat_dim2d_length(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    ASSERT_EQ(mat._dim[1], 1);
    return mat._dim[3];
}

INT Mat_dim1d(Mat_t mat) {
    ASSERT_EQ(mat._dim[0], 1);
    ASSERT_EQ(mat._dim[1], 1);
    ASSERT_EQ(mat._dim[2], 1);
    return mat._dim[3];
}

FLOAT* Mat_get_ptr_4d(Mat_t mat, INT out, INT in, INT height, INT width) {
    MATCHK_4D(mat);
    ASSERT(Mat_dim4d_out(mat) > out);
    ASSERT(Mat_dim4d_in(mat) > in);
    ASSERT(Mat_dim4d_height(mat) > height);
    ASSERT(Mat_dim4d_width(mat) > width);
    ASSERT(out >= 0 && in >= 0 && height >= 0 && width >= 0);

    INT idx = out * (mat._dim[1] * mat._dim[2] * mat._dim[3]) +
              in * (mat._dim[2] * mat._dim[3]) +
              height * mat._dim[3] + width;

    return &mat._data[idx];
}

void Mat_set_value_4d(Mat_t mat, INT in, INT out, INT height, INT width, FLOAT val) {
    *Mat_get_ptr_4d(mat, in, out, height, width) = val;
}

extern FLOAT* Mat_get_ptr_3d(Mat_t mat, INT channel, INT height, INT width) {
    MATCHK_3D(mat);
    ASSERT(Mat_dim3d_channel(mat) > channel);
    ASSERT(Mat_dim3d_height(mat) > height);
    ASSERT(Mat_dim3d_width(mat) > width);
    ASSERT(channel >= 0 && height >= 0 && width >= 0);

    INT idx = channel * (mat._dim[2] * mat._dim[3]) +
              height * mat._dim[3] + width;

    return &mat._data[idx];
}

void Mat_set_value_3d(Mat_t mat, INT channel, INT height, INT width, FLOAT val) {
    *Mat_get_ptr_3d(mat, channel, height, width) = val;
}

extern FLOAT* Mat_get_ptr_2d(Mat_t mat, INT channel, INT length) {
    MATCHK_2D(mat);
    ASSERT(Mat_dim2d_channel(mat) > channel);
    ASSERT(Mat_dim2d_length(mat) > length);
    ASSERT(channel >= 0 && length >= 0);

    return &mat._data[channel * mat._dim[3] + length];
}

void Mat_set_value_2d(Mat_t mat, INT channel, INT length, FLOAT val) {
    *Mat_get_ptr_2d(mat, channel, length) = val;
}

extern FLOAT* Mat_get_ptr_1d(Mat_t mat, INT length) {
    MATCHK_1D(mat);
    ASSERT(Mat_dim1d(mat) > length);
    ASSERT(length >= 0);

    return &mat._data[length];
}

void Mat_set_value_1d(Mat_t mat, INT length, FLOAT val) {
    *Mat_get_ptr_1d(mat, length) = val;
}

/***********************************************************************************/
/*************************************  OP  ****************************************/
/***********************************************************************************/

FLOAT _Op_scalar_product_(FLOAT* v1, FLOAT* v2, INT n) {
    FLOAT to_ret = 0;
    for(INT i = 0; i < n; i++) {
        to_ret += v1[i] * v2[i];
    }
    return to_ret;
}

void Op_multiply_mat_vec(Mat_t mat, Mat_t vec, Mat_t out) {
    MATCHK_2D(mat);
    MATCHK_2D(out);
    MATCHK_1D(vec);

    INT product_size = Mat_dim1d(vec);
    INT num_products = Mat_dim2d_length(out);

    ASSERT_EQ(product_size, Mat_dim2d_length(mat));
    ASSERT_EQ(num_products, Mat_dim2d_channel(mat));

    for(INT i = 0; i < num_products; i++) {
        Mat_set_value_1d(out, i, _Op_scalar_product_(Mat_data(vec), &mat._data[i * product_size], product_size));
    }
}


void Op_sum_matrix_matrix(Mat_t mat1, Mat_t mat2, Mat_t out) {
    MATCHK(mat1);
    MATCHK(mat2);
    MATCHK(out);

    MAT_SAME_SIZE(mat1, out);
    MAT_SAME_SIZE(mat2, out);

    for(INT i = 0; i < Mat_size(out); i++) {
        Mat_data(out)[i] = Mat_data(mat1)[i] + Mat_data(mat2)[i];
    }
}

void Op_sum_matrix_matrix_inplace(Mat_t mat_in, Mat_t mat_in_out) {
    Op_sum_matrix_matrix(mat_in, mat_in_out, mat_in_out);
}

void Op_apply_bias(Mat_t mat, Mat_t vec, Mat_t out, INT mat_size_over_vec) {
    MATCHK(mat);
    MATCHK_1D(vec);
    MATCHK(out);
    MAT_SAME_SIZE(mat, out);

    INT idx = 0;
    for(INT v_idx = 0; v_idx < Mat_dim1d(vec); v_idx++) {
        for(INT m_idx = 0; m_idx < mat_size_over_vec; m_idx++) {
            Mat_data(out)[idx] = Mat_data(mat)[idx] + Mat_data(vec)[v_idx];
            idx++;
        }
    }
}

void Op_apply_bias_inplace(Mat_t mat, Mat_t vec, INT mat_size_over_vec) {
    Op_apply_bias(mat, vec, mat, mat_size_over_vec);
}

INT _Op_compute_size(INT mat_size, INT kernel, INT padding, INT stride) {
    return (mat_size - kernel + 2 * padding) / stride + 1;
}

void _Op_check_size(INT in_size, INT out_size, INT kernel, INT padding, INT stride) {
    ASSERT_EQ(out_size, _Op_compute_size(in_size, kernel, padding, stride));
}

// no support for padding yet...
void _Op_pooling(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex,
                 FLOAT (*pooling_function)(Mat_t, int, int, int, int, int, FLOAT), FLOAT p) {
    MATCHK(mat);
    MATCHK(out);

    ASSERT_EQ(Mat_dim3d_channel(mat), Mat_dim3d_channel(out));
    _Op_check_size(Mat_dim3d_height(mat), Mat_dim3d_height(out), kernely, 0, stridey);
    _Op_check_size(Mat_dim3d_width(mat), Mat_dim3d_width(out), kernelx, 0, stridex);

// #pragma omp parallel for
    for(INT channel = 0; channel < Mat_dim3d_channel(mat); channel++) {
        INT h_in = 0;
        for(INT h = 0; h < Mat_dim3d_height(out); h++) {
            INT w_in = 0;
            for(INT w = 0; w < Mat_dim3d_width(out); w++) {
                FLOAT value = pooling_function(mat, channel, h_in, w_in, kernely, kernelx, p);
                Mat_set_value_3d(out, channel, h, w, value);
                w_in += stridex;
            }
            h_in += stridey;
        }
    }
}

FLOAT _Op_maxpool_func(Mat_t mat, INT channel, INT y, INT x, INT ky, INT kx, FLOAT _) {
    UNUSED(_);
    FLOAT* row = Mat_get_ptr_3d(mat, channel, y, x);
    FLOAT max = *row;
    for(INT i = 0; i < ky; i++) {
        row = Mat_get_ptr_3d(mat, channel, y + i, x);
        for(INT j = 0; j < kx; j++) {
            if(row[j] > max) {
                max = row[j];
            }
        }
    }
    return max;
}

void Op_maxpool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex) {
    _Op_pooling(mat, out, kernely, kernelx, stridey, stridex, _Op_maxpool_func, 0);
}

FLOAT _Op_avgpool_func(Mat_t mat, INT channel, INT y, INT x, INT ky, INT kx, FLOAT _) {
    UNUSED(_);
    FLOAT sum = 0;
    for(INT i = 0; i < ky; i++) {
        FLOAT* row = Mat_get_ptr_3d(mat, channel, y + i, x);
        for(INT j = 0; j < kx; j++) {
            sum += row[j];
        }
    }
    return sum / (FLOAT)(kx * ky);
}

void Op_avgpool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex) {
    _Op_pooling(mat, out, kernely, kernelx, stridey, stridex, _Op_avgpool_func, 0);
}

FLOAT _Op_lppool_func(Mat_t mat, INT channel, INT y, INT x, INT ky, INT kx, FLOAT power) {
    FLOAT sum = 0;
    for(INT i = 0; i < ky; i++) {
        FLOAT* row = Mat_get_ptr_3d(mat, channel, y + i, x);
        for(INT j = 0; j < kx; j++) {
            sum += POW(row[j], power);
        }
    }
    return POW(sum, 1 / power);
}

void Op_lppool(Mat_t mat, Mat_t out, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT power) {
    _Op_pooling(mat, out, kernely, kernelx, stridey, stridex, _Op_lppool_func, power);
}

void _Op_check_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down) {
    MATCHK_3D(mat);
    MATCHK_3D(out);
    ASSERT_EQ(Mat_dim3d_channel(mat), Mat_dim3d_channel(out));

    INT mat_height = Mat_dim3d_height(mat);
    INT mat_width = Mat_dim3d_width(mat);

    ASSERT_EQ(Mat_dim3d_height(out), mat_height + up + down);
    ASSERT_EQ(Mat_dim3d_width(out), mat_width + left + right);
}

void Op_constant_pad(Mat_t mat, Mat_t out, FLOAT constant, INT left, INT right, INT up, INT down) {
    _Op_check_pad(mat, out, left, right, up, down);
    INT mat_height = Mat_dim3d_height(mat);
    INT mat_width = Mat_dim3d_width(mat);

    for(INT i = 0; i < Mat_size(out); i++) {
        Mat_data(out)[i] = constant;
    }

    for(INT channel = 0; channel < Mat_dim3d_channel(mat); channel++) {
        for(INT y = 0; y < mat_height; y++) {
            INT h = y + up;
            FLOAT* copy_from = Mat_get_ptr_3d(mat, channel, y, 0);
            FLOAT* copy_to = Mat_get_ptr_3d(out, channel, h, left);
            memcpy(copy_to, copy_from, (size_t)mat_width * sizeof(FLOAT));
        }
    }
}

void Op_zero_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down) {
    Op_constant_pad(mat, out, 0, up, down, left, right);
}

void Op_replication_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down) {
    _Op_check_pad(mat, out, left, right, up, down);
    INT mat_height = Mat_dim3d_height(mat);
    INT mat_width = Mat_dim3d_width(mat);
    INT out_height = Mat_dim3d_height(out);
    INT out_width = Mat_dim3d_width(out);

    for(INT channel = 0; channel < Mat_dim3d_channel(mat); channel++) {
        for(INT y = 0; y < out_height; y++) {
            INT nearest_row = y < up ? 0 : (y > out_height - down ? mat_height - 1 : y - up);
            FLOAT* copy_from = Mat_get_ptr_3d(mat, channel, nearest_row, 0);
            FLOAT* copy_to = Mat_get_ptr_3d(out, channel, y, 0);

            // left
            FLOAT left_flt = copy_from[0];
            for(INT x = 0; x < left; x++) {
                copy_to[x] = left_flt;
            }

            // center
            memcpy(&copy_to[left], copy_from, (size_t)mat_width * sizeof(FLOAT));

            // right
            FLOAT right_flt = copy_from[mat_width - 1];
            for(INT x = left + mat_width; x < out_width; x++) {
                copy_to[x] = right_flt;
            }
        }
    }
}

INT _Op_reflection_index(INT index, INT width) {
    index = abs(index) % (width - 1);
    return MIN(index, width - 1 - index);
}

void Op_reflection_pad(Mat_t mat, Mat_t out, INT left, INT right, INT up, INT down) {
    _Op_check_pad(mat, out, left, right, up, down);
    INT mat_height = Mat_dim3d_height(mat);
    INT mat_width = Mat_dim3d_width(mat);
    INT out_height = Mat_dim3d_height(out);
    INT out_width = Mat_dim3d_width(out);

    for(INT channel = 0; channel < Mat_dim3d_channel(mat); channel++) {
        for(INT y = 0; y < out_height; y++) {
            INT nearest_row = y < up ? 0 : (y > out_height - down ? mat_height - 1 : y - up);
            FLOAT* copy_from = Mat_get_ptr_3d(mat, channel, nearest_row, 0);
            FLOAT* copy_to = Mat_get_ptr_3d(out, channel, y, 0);

            for(INT x = 0; x < out_width; x++) {
                INT copy_from_idx = x - left;
                copy_to[x] = copy_from[_Op_reflection_index(copy_from_idx, mat_width)];
            }
        }
    }
}

void _Op_conv_multiplication(Mat_t input, Mat_t kernel, Mat_t output, INT y0, INT x0) {
    MATCHK_1D(output);

    for(INT outchannel = 0; outchannel < Mat_dim1d(output); outchannel++) {
        FLOAT val = 0;
        for(INT inchannel = 0; inchannel < Mat_dim3d_channel(input); inchannel++) {
            for(INT y = 0; y < Mat_dim4d_height(kernel); y++) {
                FLOAT* vec1 = Mat_get_ptr_3d(input, inchannel, y0 + y, x0);
                FLOAT* vec2 = Mat_get_ptr_4d(kernel, outchannel, inchannel, y, 0);
                val += _Op_scalar_product_(vec1, vec2, Mat_dim4d_width(kernel));
            }
        }
        Mat_set_value_1d(output, outchannel, val);
    }
}

void _Op_conv_multiplication_padding(Mat_t input, Mat_t kernel, Mat_t output, INT y0, INT x0, INT max_y, INT max_x) {
    // for "special cases" on padded edges ONLY
    MATCHK_1D(output);

    INT kx = Mat_dim4d_width(kernel);
    INT ky = Mat_dim4d_height(kernel);

    ASSERT(y0 < 0 || x0 < 0 || y0 + ky >= max_y || x0 + kx > max_x);

    // padded left/right ALOT
    if(y0 + ky <= 0 || x0 + kx <= 0 || y0 >= max_y || x0 >= max_x) {
        for(INT i = 0; i < Mat_size(output); i++) {
            Mat_data(output)[i] = 0;
        }
        return;
    }

    // padded left / right
    // by default, scalar product of length kx,
    // starts at input[..., x0] and kernel[..., 0]
    INT xk = 0;

    // if padded on right, just lower the length
    // of scalar product
    if(x0 + kx > max_x) {
        kx = max_x - x0;
    }

    // but if padded on left, then fix where
    // scalar product starts and also lower
    // ther length of scalar product
    else if(x0 < 0) {
        xk = -x0;
        kx += x0;
        x0 = 0;
    }

    for(INT outchannel = 0; outchannel < Mat_dim1d(output); outchannel++) {
        FLOAT val = 0;
        for(INT inchannel = 0; inchannel < Mat_dim3d_channel(input); inchannel++) {
            for(INT y = 0; y < ky; y++) {
                // padded top / bottom
                if(y0 + y < 0 || y + y0 >= max_y)
                    continue;

                FLOAT* vec1 = Mat_get_ptr_3d(input, inchannel, y0 + y, x0);
                FLOAT* vec2 = Mat_get_ptr_4d(kernel, outchannel, inchannel, y, xk);
                val += _Op_scalar_product_(vec1, vec2, kx);
            }
        }
        Mat_set_value_1d(output, outchannel, val);
    }
}

void Op_convolution_2d(Mat_t input, Mat_t kernel, Mat_t output, INT pady, INT padx, INT stridey, INT stridex) {
    MATCHK_3D(input);
    MATCHK_3D(output);
    MATCHK_4D(kernel);

    INT input_channels = Mat_dim3d_channel(input);
    INT output_channels = Mat_dim3d_channel(output);

    INT ky = Mat_dim4d_height(kernel);
    INT kx = Mat_dim4d_width(kernel);

    INT input_height = Mat_dim3d_height(input);
    INT input_width = Mat_dim3d_width(input);
    INT output_height = Mat_dim3d_height(output);
    INT output_width = Mat_dim3d_width(output);

    ASSERT(input_width >= kx);
    ASSERT(input_height >= ky);

    ASSERT_EQ(input_channels, Mat_dim4d_in(kernel));
    ASSERT_EQ(output_channels, Mat_dim4d_out(kernel));

    _Op_check_size(input_height, output_height, ky, pady, stridey);
    _Op_check_size(input_width, output_width, kx, padx, stridex);

    FLOAT temp_buffer[output_channels];
    Mat_t temp = Mat_make_1d(output_channels, temp_buffer);

// #pragma omp parallel for
    for(INT y = 0; y < output_height; y += 1) {
        for(INT x = 0; x < output_width; x += 1) {

            INT yp = y * stridey - pady;
            INT xp = x * stridex - padx;

            if(yp < 0 || xp < 0 || yp + ky > input_height || xp + kx > input_width) {
                _Op_conv_multiplication_padding(input, kernel, temp, yp, xp, input_height, input_width);
            }
            else {
                _Op_conv_multiplication(input, kernel, temp, yp, xp);
            }

            for(INT channel = 0; channel < output_channels; channel++) {
                FLOAT val = *Mat_get_ptr_1d(temp, channel);
                Mat_set_value_3d(output, channel, y, x, val);
            }
        }
    }
}

FLOAT Op_activate_ELU(FLOAT val, FLOAT* params) {
    return val >= 0 ? val : params[0] * (EXP(val) - 1);
}

FLOAT Op_activate_HardShrink(FLOAT val, FLOAT* params) {
    return (val > params[0] || val < -params[0]) ? val : 0;
}

FLOAT Op_activate_HardTanh(FLOAT val, FLOAT* params) {
    return val < params[0] ? params[0] : (val > params[1] ? params[1] : val);
}

FLOAT Op_activate_LeakyReLU(FLOAT val, FLOAT* params) {
    return val >= 0 ? val : params[0] * val;
}

FLOAT Op_activate_LogSigmoid(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return LOG(1 / (1 + EXP(-val)));
}

FLOAT Op_activate_PReLU(FLOAT val, FLOAT* params) {
    return val >= 0 ? val : params[0] * val;
}

FLOAT Op_activate_ReLU(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return val < 0 ? 0 : val;
}

FLOAT Op_activate_ReLU6(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return val < 0 ? 0 : (val > 6 ? 6 : val);
}

FLOAT Op_activate_RReLU(FLOAT val, FLOAT* params) {
    FLOAT randmax = (FLOAT)RAND_MAX;
    FLOAT r = (FLOAT)rand() / (randmax / (params[1] - params[0])) + params[0];
    return val > 0 ? val : r * val;
}

FLOAT Op_activate_SeLU(FLOAT val, FLOAT* _) {
    UNUSED(_);
    const FLOAT alpha = (FLOAT)1.673263242354377284817042991671;
    const FLOAT scale = (FLOAT)1.0507009873554804934193349852946;
    return scale * (val > 0 ? val : alpha * (EXP(val) - 1));
}

FLOAT Op_activate_Sigmoid(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return 1 / (1 + EXP(-val));
}

FLOAT Op_activate_SoftPlus(FLOAT val, FLOAT* params) {
    return val > params[1] ? val : LOG(1 + EXP(params[0] * val)) / params[0];
}

FLOAT Op_activate_SoftShrink(FLOAT val, FLOAT* params) {
    return val > params[0] ? val - params[0] : (val < -params[0] ? val + params[0] : 0);
}

FLOAT Op_activate_SoftSign(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return val / (1 + FABS(val));
}

FLOAT Op_activate_Tanh(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return (EXP(val) - EXP(-val)) / (EXP(val) + EXP(-val));
}

FLOAT Op_activate_TanhShrink(FLOAT val, FLOAT* _) {
    UNUSED(_);
    return val - Op_activate_Tanh(val, _);
}

FLOAT Op_activate_Threshold(FLOAT val, FLOAT* params) {
    return val > params[0] ? val : params[1];
}


/***********************************************************************************/
/************************************* LAYER ***************************************/
/***********************************************************************************/

Layer_t _Layer_make_empty(layer_type type, Mat_t input) {
    Layer_t to_ret;
    to_ret._input = input;
    to_ret._weights = Mat_make_empty();
    to_ret._output = Mat_make_empty();
    to_ret._type = type;
    to_ret._other_mat = Mat_make_empty();
    memset(to_ret._parameters, 0, sizeof(FLOAT) * NUM_LAYER_PARAMS);

    return to_ret;
}

Layer_t _Layer_make_common(layer_type type, Mat_t input, FLOAT* output) {
    Layer_t to_ret = _Layer_make_empty(type, input);
    to_ret._output = Mat_make_3d(Mat_dim3d_channel(input),
                                 Mat_dim3d_height(input),
                                 Mat_dim3d_width(input),
                                 output);
    return to_ret;
}

Layer_t Layer_make_skip(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_Skip, input_container, output);
}

Layer_t Layer_make_conv_2d(Mat_t input_container, INT out_channels,
                           INT kernely, INT kernelx, INT paddingy, INT paddingx, INT stridey, INT stridex,
                           FLOAT* weights, FLOAT* bias, FLOAT* output) {
    Mat_t weights_mat = Mat_make_4d(out_channels, Mat_dim3d_channel(input_container), kernely, kernelx, weights);

    INT out_height = _Op_compute_size(Mat_dim3d_height(input_container), kernely, paddingy, stridey);
    INT out_width = _Op_compute_size(Mat_dim3d_width(input_container), kernelx, paddingx, stridex);

    Mat_t container = Mat_make_3d(out_channels, out_height, out_width, output);

    Mat_t bias_mat = Mat_make_empty();
    if(bias != NULL) {
        bias_mat = Mat_make_1d(out_channels, bias);
    }

    Layer_t to_ret;
    to_ret._input = input_container;
    to_ret._weights = weights_mat;
    to_ret._output = container;
    to_ret._other_mat = bias_mat;
    to_ret._type = LAYER_Conv_2D;

    to_ret._parameters[0] = (FLOAT)paddingy;
    to_ret._parameters[1] = (FLOAT)paddingx;
    to_ret._parameters[2] = (FLOAT)stridey;
    to_ret._parameters[3] = (FLOAT)stridex;

    return to_ret;
}

Layer_t Layer_make_dense(Mat_t input_container, INT out_size, FLOAT* weights, FLOAT* bias, FLOAT* output) {
    Mat_t weights_mat = Mat_make_2d(out_size, Mat_dim1d(input_container), weights);

    Layer_t to_ret;
    to_ret._input = input_container;
    to_ret._output = Mat_make_1d(out_size, output);
    to_ret._weights = weights_mat;
    to_ret._other_mat = Mat_make_empty();
    to_ret._type = LAYER_Dense;

    if(bias != NULL) {
        to_ret._other_mat = Mat_make_1d(out_size, bias);
    }

    return to_ret;
}

Layer_t Layer_make_elu(Mat_t input_container, FLOAT alpha, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_ELU, input_container, output);
    to_ret._parameters[0] = alpha;
    return to_ret;
}

Layer_t Layer_make_hardshrink(Mat_t input_container, FLOAT lambda, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_HardShrink, input_container, output);
    to_ret._parameters[0] = lambda;
    return to_ret;
}

Layer_t Layer_make_hardtanh(Mat_t input_container, FLOAT min_val, FLOAT max_val, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_HardTanh, input_container, output);
    to_ret._parameters[0] = min_val;
    to_ret._parameters[1] = max_val;
    return to_ret;
}

Layer_t Layer_make_leakyrelu(Mat_t input_container, FLOAT negative_slope, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_LeakyReLU, input_container, output);
    to_ret._parameters[0] = negative_slope;
    return to_ret;
}

Layer_t Layer_make_logsigmoid(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_LogSigmoid, input_container, output);
}

Layer_t Layer_make_prelu(Mat_t input_container, FLOAT a, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_PReLU, input_container, output);
    to_ret._parameters[0] = a;
    return to_ret;
}

Layer_t Layer_make_relu(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_ReLU, input_container, output);
}

Layer_t Layer_make_relu6(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_ReLU6, input_container, output);
}

Layer_t Layer_make_rrelu(Mat_t input_container, FLOAT lower, FLOAT upper, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_RReLU, input_container, output);
    to_ret._parameters[0] = lower;
    to_ret._parameters[1] = upper;
    return to_ret;
}

Layer_t Layer_make_selu(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_SeLU, input_container, output);
}

Layer_t Layer_make_sigmoid(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_Sigmoid, input_container, output);
}

Layer_t Layer_make_softplus(Mat_t input_container, FLOAT beta, FLOAT threshold, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_SoftPlus, input_container, output);
    to_ret._parameters[0] = beta;
    to_ret._parameters[1] = threshold;
    return to_ret;
}

Layer_t Layer_make_softshrink(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_SoftShrink, input_container, output);
}

Layer_t Layer_make_softsign(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_SoftSign, input_container, output);
}

Layer_t Layer_make_tanh(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_Tanh, input_container, output);
}

Layer_t Layer_make_tanhshrink(Mat_t input_container, FLOAT* output) {
    return _Layer_make_common(LAYER_TanhShrink, input_container, output);
}

Layer_t Layer_make_threshold(Mat_t input_container, FLOAT threshold, FLOAT value, FLOAT* output) {
    Layer_t to_ret = _Layer_make_common(LAYER_Threshold, input_container, output);
    to_ret._parameters[0] = threshold;
    to_ret._parameters[1] = value;
    return to_ret;
}

Layer_t _Layer_make_pool(Mat_t input, layer_type lt,
                         INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output) {
    Layer_t to_ret = _Layer_make_empty(lt, input);
    INT height = _Op_compute_size(Mat_dim3d_height(input), kernely, 0, stridey);
    INT width = _Op_compute_size(Mat_dim3d_width(input), kernelx, 0, stridex);

    to_ret._output = Mat_make_3d(Mat_dim3d_channel(input), height, width, output);

    to_ret._parameters[0] = (FLOAT)kernely;
    to_ret._parameters[1] = (FLOAT)kernelx;
    to_ret._parameters[2] = (FLOAT)stridey;
    to_ret._parameters[3] = (FLOAT)stridex;

    return to_ret;
}

Layer_t Layer_make_maxpool(Mat_t input_container, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output) {
    return _Layer_make_pool(input_container, LAYER_MaxPool, kernely, kernelx, stridey, stridex, output);
}

Layer_t Layer_make_avgpool(Mat_t input_container, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output) {
    return _Layer_make_pool(input_container, LAYER_AvgPool, kernely, kernelx, stridey, stridex, output);
}

Layer_t Layer_make_lppool(Mat_t input_container, FLOAT power, INT kernely, INT kernelx, INT stridey, INT stridex, FLOAT* output) {
    Layer_t to_ret = _Layer_make_pool(input_container, LAYER_LpPool, kernely, kernelx, stridey, stridex, output);
    to_ret._parameters[4] = power;
    return to_ret;
}

Layer_t _Layer_make_pad(Mat_t input_container, layer_type lt, INT left, INT right, INT up, INT down, FLOAT* output) {
    Layer_t to_ret = _Layer_make_empty(lt, input_container);

    INT width = Mat_dim4d_width(input_container) + left + right;
    INT height = Mat_dim4d_width(input_container) + up + down;

    to_ret._output = Mat_make_4d(input_container._dim[0], input_container._dim[1], width, height, output);

    to_ret._parameters[0] = (FLOAT)left;
    to_ret._parameters[1] = (FLOAT)right;
    to_ret._parameters[2] = (FLOAT)up;
    to_ret._parameters[3] = (FLOAT)down;
    return to_ret;
}

Layer_t Layer_make_replication_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output) {
    return _Layer_make_pad(input_container, LAYER_ReplicationPad, left, right, up, down, output);
}

Layer_t Layer_make_reflection_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output) {
    return _Layer_make_pad(input_container, LAYER_ReflectionPad, left, right, up, down, output);
}

Layer_t Layer_make_constant_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT constant, FLOAT* output) {
    Layer_t to_ret = _Layer_make_pad(input_container, LAYER_ConstantPad, left, right, up, down, output);
    to_ret._parameters[5] = constant;
    return to_ret;
}

Layer_t Layer_make_zero_pad(Mat_t input_container, INT left, INT right, INT up, INT down, FLOAT* output) {
    return _Layer_make_pad(input_container, LAYER_ZeroPad, left, right, up, down, output);
}

Layer_t Layer_make_flatten(Mat_t input_container, FLOAT* output) {
    Layer_t to_ret = _Layer_make_empty(LAYER_Flatten, input_container);
    INT size = Mat_size(input_container);
    to_ret._output = Mat_make_1d(size, output);
    return to_ret;
}

Layer_t Layer_make_concat(Mat_t m1, Mat_t m2, FLOAT* output) {
    ASSERT_EQ(Mat_dim3d_height(m1), Mat_dim3d_height(m2));
    ASSERT_EQ(Mat_dim3d_width(m1), Mat_dim3d_width(m2));

    Layer_t to_ret = _Layer_make_empty(LAYER_Concat, m1);
    to_ret._other_mat = m2;
    to_ret._output = Mat_make_3d(Mat_dim3d_channel(m1) + Mat_dim3d_channel(m2),
                                    Mat_dim3d_height(m1), Mat_dim3d_width(m1), output);
    return to_ret;
}

void Layer_print(Layer_t layer) {
    const char* layer_name = layer_names[layer._type];
    const char* bias = (Layer_has_bias(layer) ? "with" : "no");

    char layer_additional[200];
    layer_additional[0] = 0;

    INT p[4];
    INT additional_end = 0;

    switch (layer._type) {
    case LAYER_Conv_2D:
        p[0] = Layer_int_param(layer, 0);
        p[1] = Layer_int_param(layer, 1);
        p[2] = Layer_int_param(layer, 2);
        p[3] = Layer_int_param(layer, 3);
        SPRINTF(layer_additional, "kernel: (%d, %d), padding (%d, %d), stride (%d, %d), %s bias",
                Mat_dim4d_height(layer._weights), Mat_dim4d_width(layer._weights),
                p[0], p[1], p[2], p[3], bias);
        break;
    case LAYER_Dense:
        SPRINTF(layer_additional, "%s bias", bias);
        break;
    case LAYER_ELU:
        SPRINTF(layer_additional, "alpha=" FLOAT_FORMAT, layer._parameters[0]);
        break;
    case LAYER_HardShrink:
        SPRINTF(layer_additional, "lamda=" FLOAT_FORMAT, layer._parameters[0]);
        break;
    case LAYER_HardTanh:
        SPRINTF(layer_additional, "alpha=" FLOAT_FORMAT ", max=" FLOAT_FORMAT,
                layer._parameters[0], layer._parameters[1]);
        break;
    case LAYER_LeakyReLU:
        SPRINTF(layer_additional, "neg_slope=" FLOAT_FORMAT, layer._parameters[0]);
        break;
    case LAYER_PReLU:
        SPRINTF(layer_additional, "a=" FLOAT_FORMAT, layer._parameters[0]);
        break;
    case LAYER_RReLU:
        SPRINTF(layer_additional, "lower=" FLOAT_FORMAT ", upper=" FLOAT_FORMAT,
                layer._parameters[0], layer._parameters[1]);
        break;
    case LAYER_SoftPlus:
        SPRINTF(layer_additional, "beta=" FLOAT_FORMAT ", threshold=" FLOAT_FORMAT,
                layer._parameters[0], layer._parameters[1]);
        break;
    case LAYER_Threshold:
        SPRINTF(layer_additional, "threshold=" FLOAT_FORMAT ", value=" FLOAT_FORMAT,
                layer._parameters[0], layer._parameters[1]);
        break;
    case LAYER_ConstantPad:
        SPRINTF(layer_additional,  "constant=" FLOAT_FORMAT ",", layer._parameters[4]);
        additional_end = (INT)strlen(layer_additional);
        /* fall through */
    case LAYER_ReflectionPad:
    case LAYER_ReplicationPad:
    case LAYER_ZeroPad:
        SPRINTF(&layer_additional[additional_end],
                "padding=(" FLOAT_FORMAT "," FLOAT_FORMAT "," FLOAT_FORMAT "," FLOAT_FORMAT ")",
                layer._parameters[0], layer._parameters[1], layer._parameters[2], layer._parameters[3]);
        break;
    case LAYER_LpPool:
        SPRINTF(layer_additional,  "constant=" FLOAT_FORMAT ",", layer._parameters[4]);
        additional_end = (INT)strlen(layer_additional);
        /* fall through */
    case LAYER_MaxPool:
    case LAYER_AvgPool:
        p[0] = Layer_int_param(layer, 0);
        p[1] = Layer_int_param(layer, 1);
        p[2] = Layer_int_param(layer, 2);
        p[3] = Layer_int_param(layer, 3);
        SPRINTF(layer_additional, "kernel: (%d, %d), padding (%d, %d), stride (%d, %d)",
                p[0], p[1], 0, 0, p[2], p[3]);
        break;
    default:
        break;
    }

    char size_info[100];
    size_info[0] = 0;
    SPRINTF(size_info, "(%d:%d:%d)", Mat_dim3d_channel(layer._output),
                                     Mat_dim3d_height(layer._output),
                                     Mat_dim3d_width(layer._output));

    PRINTF(LAYER_STRING_FORMAT, layer_name, size_info, layer_additional);
}

INT Layer_has_bias(Layer_t layer) {
    switch (layer._type) {
    case LAYER_Conv_2D:
    case LAYER_Dense:
        return Mat_is_allocated(layer._other_mat);
    default:
        return 0;
    }
}

Mat_t Layer_output(Layer_t layer) {
    return layer._output;
}

Mat_t Layer_input(Layer_t layer) {
    return layer._input;
}

INT Layer_int_param(Layer_t layer, INT idx) {
    ASSERT(idx < NUM_LAYER_PARAMS);
    return (INT)layer._parameters[idx];
}

void _Layer_apply_dense(Layer_t layer) {
    Op_multiply_mat_vec(layer._weights, Layer_input(layer), layer._output);
    if(Layer_has_bias(layer)) {
        Op_apply_bias_inplace(layer._output, layer._other_mat, 1);
    }
}

void _Layer_apply_conv2d(Layer_t layer) {
    INT pady = Layer_int_param(layer, 0);
    INT padx = Layer_int_param(layer, 1);
    INT stry = Layer_int_param(layer, 2);
    INT strx = Layer_int_param(layer, 3);

    Op_convolution_2d(Layer_input(layer), layer._weights, layer._output, pady, padx, stry, strx);
    if(Layer_has_bias(layer)) {
        INT mat_per_vec = Mat_dim3d_height(layer._output) * Mat_dim3d_width(layer._output);
        Op_apply_bias_inplace(layer._output, layer._other_mat, mat_per_vec);
    }
}

void _Layer_activate(FLOAT (*activate_func)(FLOAT val, FLOAT* params), Layer_t layer) {
    FLOAT* params = (FLOAT*)layer._parameters;
    Mat_t input = Layer_input(layer);
    for(int i = 0; i < Mat_size(input); i++) {
        Mat_data(layer._output)[i] = activate_func(Mat_data(input)[i], params);
    }
}

void _Copy_if_needed(FLOAT* dst, FLOAT* src, INT num) {
    ASSERT(num >= 0);
    if(dst != src) {
        memcpy(dst, src, (size_t)num * sizeof(FLOAT));
    }
}

void _Layer_apply_flatten(Layer_t layer) {
    _Copy_if_needed(Layer_output(layer)._data, Layer_input(layer)._data, Mat_size(Layer_input(layer)));
}

void _Layer_apply_concat(Layer_t layer) {
    FLOAT* dest = Layer_output(layer)._data;
    Mat_t in1 = Layer_input(layer);
    Mat_t in2 = layer._other_mat;

    _Copy_if_needed(dest, in1._data, Mat_size(in1));
    _Copy_if_needed(&dest[Mat_size(in1)], in2._data, Mat_size(in2));
}

void _Layer_apply_skip(Layer_t layer) {
    _Layer_apply_flatten(layer);
}

void Layer_apply(Layer_t layer) {
    switch (layer._type) {
    case LAYER_Conv_2D:
        _Layer_apply_conv2d(layer);
        break;
    case LAYER_Skip:
        _Layer_apply_skip(layer);
        break;
    case LAYER_Dense:
        _Layer_apply_dense(layer);
        break;
    case LAYER_ELU:
        _Layer_activate(Op_activate_ELU, layer);
        break;
    case LAYER_HardShrink:
        _Layer_activate(Op_activate_HardShrink, layer);
        break;
    case LAYER_HardTanh:
        _Layer_activate(Op_activate_HardTanh, layer);
        break;
    case LAYER_LeakyReLU:
        _Layer_activate(Op_activate_LeakyReLU, layer);
        break;
    case LAYER_LogSigmoid:
        _Layer_activate(Op_activate_LogSigmoid, layer);
        break;
    case LAYER_PReLU:
        _Layer_activate(Op_activate_PReLU, layer);
        break;
    case LAYER_ReLU:
        _Layer_activate(Op_activate_ReLU, layer);
        break;
    case LAYER_ReLU6:
        _Layer_activate(Op_activate_ReLU6, layer);
        break;
    case LAYER_RReLU:
        _Layer_activate(Op_activate_RReLU, layer);
        break;
    case LAYER_SeLU:
        _Layer_activate(Op_activate_SeLU, layer);
        break;
    case LAYER_Sigmoid:
        _Layer_activate(Op_activate_Sigmoid, layer);
        break;
    case LAYER_SoftPlus:
        _Layer_activate(Op_activate_SoftPlus, layer);
        break;
    case LAYER_SoftShrink:
        _Layer_activate(Op_activate_SoftShrink, layer);
        break;
    case LAYER_SoftSign:
        _Layer_activate(Op_activate_SoftSign, layer);
        break;
    case LAYER_Tanh:
        _Layer_activate(Op_activate_Tanh, layer);
        break;
    case LAYER_TanhShrink:
        _Layer_activate(Op_activate_TanhShrink, layer);
        break;
    case LAYER_Threshold:
        _Layer_activate(Op_activate_Threshold, layer);
        break;
    case LAYER_MaxPool:
        Op_maxpool(Layer_input(layer), layer._output, Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_AvgPool:
        Op_avgpool(Layer_input(layer), layer._output, Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_LpPool:
        Op_lppool(Layer_input(layer), layer._output, Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3), layer._parameters[4]);
        break;
    case LAYER_ReplicationPad:
        Op_replication_pad(Layer_input(layer), layer._output,Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_ReflectionPad:
        Op_reflection_pad(Layer_input(layer), layer._output,Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_ZeroPad:
        Op_zero_pad(Layer_input(layer), layer._output, Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_ConstantPad:
        Op_constant_pad(Layer_input(layer), layer._output, layer._parameters[4], Layer_int_param(layer, 0), Layer_int_param(layer, 1),
                                Layer_int_param(layer, 2), Layer_int_param(layer, 3));
        break;
    case LAYER_Flatten:
        _Layer_apply_flatten(layer);
        break;
    case LAYER_Concat:
        _Layer_apply_concat(layer);
        break;
    default:
        UNREACHABLE();
    }
}


/***********************************************************************************/
/************************************ NETWORK **************************************/
/***********************************************************************************/

Network_t Network_make(Mat_t input, INT size, Layer_t* layer_store) {
    Network_t network;
    network.input = input;
    network.size = size;
    network.layers = layer_store;
    network.verbose = 0;
    return network;
}

INT Network_apply(Network_t network) {
    volatile INT i;
    for(i = 0; i < network.size; i++) {
        if(!EXECUTION_OK) {
            return 0;
        }

        if(network.verbose) {
            Mat_print_1d(Mat_flatten(Layer_input(network.layers[i])));
        }
        Layer_apply(network.layers[i]);
    }

    if(network.verbose) {
        Mat_print_1d(Mat_flatten(Network_output(network)));
    }
    return 1;
}

Mat_t Network_output(Network_t network) {
    return Layer_output(network.layers[network.size - 1]);
}

Mat_t Network_input(Network_t network) {
    return network.input;
}

void Network_print(Network_t network) {
    PRINTF("----- NETWORK --------------------------------");
    PRINTF("----------------------------------------------\n");

    char size_info[100];
    size_info[0] = 0;
    SPRINTF(size_info, "(%d:%d:%d)", Mat_dim3d_channel(network.input),
                                     Mat_dim3d_height(network.input),
                                     Mat_dim3d_width(network.input));

    PRINTF("000: ");
    PRINTF(LAYER_STRING_FORMAT, "", size_info, "this is input size");

    FLOAT* inputs[network.size + 1];
    inputs[0] = network.input._data;

    for(INT i = 0; i < network.size; i++) {
        Layer_t layer = network.layers[i];

        INT input_idx = -1;
        inputs[i + 1] = Layer_output(layer)._data;
        for(INT j = 0; j <= i; j++) {
            if(Layer_input(layer)._data == inputs[j]) {
                input_idx = j;
            }
        }
        ASSERT(input_idx >= 0);

        PRINTF("%03d (in %03d) :: ", i + 1, input_idx);
        Layer_print(layer);
    }
    PRINTF("----------------------------------------------");
    PRINTF("----------------------------------------------\n");
}
